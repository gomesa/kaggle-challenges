### Challenge

Name: "House Prices: Advanced Regression Techniques" <br>
URL: https://www.kaggle.com/c/house-prices-advanced-regression-techniques

### My score (Kaggle's leaderboard)
0.13054

### Main insights

- Data analysis & preprocessing play a huge role in this problem. Data is dirty. However, many features are correlated to each other, which helps fulfilling the gaps;
- ML models blending seems to lead to good results here. Even blending ensembled (surprise!) methods improve the results.
- Usefull frameworks:
	1. XGBRegressor: more efficient than sklearn GradientBoostingRegressor in terms of computing resources management;
	2. sklearn.decomposition.PCA: for PCA analysis & dimensionality reduction;
	3. sklearn.model\_selection.RandomizedSearchCV: for hyperparameter tunning (very usefull!);
	4. sklearn.pipeline.make\_pipeline: for creating ML pipelines (very usefull!);

#### Feature selection

- Filtering Method (FM): FM removes features that are correlated to the output to an extent less than a given threshold;

- Recursive Feature Elimination (RFE): RFE recursively removes useless features for a given ML model. Then, the remaining features are used on the final model.

#### Feature scaling

- Usefull reference: <https://towardsdatascience.com/scale-standardize-or-normalize-with-scikit-learn-6ccc7d176a02>
